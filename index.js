console.log("Hello");

// Operators

// Assignment Operators

// Basic Assignmnet Operator (=)
let assignmentNumber = 8;


// Arithmetic Assignment Operators

// (+)
	assignmentNumber = assignmentNumber + 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);

	// Addition Assignment Operator (+=)
		assignmentNumber += 2;
		console.log("Result of addition assignment operator: " + assignmentNumber);

	// Subtraction/ Mulitplicaiton/ Division Assignment Operator (-=, *=, /=)



// Arithmetic Operators (+, -, *, /, %)
let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
let product = x * y;
let quotient = x / y;
let modulus = x % y;

console.log("Result of subtraction operator: " + difference);
console.log("Result of multiplication operator: " + product);
console.log("Result of division operator: " + quotient);
console.log("Result of modulus operator: " + modulus);

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operator: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operator: " + pemdas);


// Increment and Decrement
let z = 1;

// Pre-Increment
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// Post-Increment
// The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);



// Pre-Decrement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// Post-Decrement
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);




// Type Coercion
// 		- type coercion is the automatic or implicit conversion of values from one data type to another

let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);



// Comparison Operators
let juan = "juan";

// Equality Operator (==)
/*
		- checks whether the operands are equal/have the same content
		- returns a boolean value
		- automatic type coercion is aplied
*/

console.log("\n\nEquality");
console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log("juan" == "juan");
console.log('juan' == juan);


// Inequality Operator (!=)
// 		-checks whether the operand are not equal/have different content

console.log("\n\nInequality");
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log("juan" != "juan");
console.log('juan' != juan);


// Strict Equality Operator (===)
//		- no type coercion happening
console.log("\n\nStrict Equality");
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log("juan" === "juan");
console.log('juan' === juan);



// Strict Inequality Operator (!==)
//		- no type coercion happening
console.log("\n\nStrict Inequality");
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log("juan" !== "juan");
console.log('juan' !== juan);


// Logical Operators
console.log("\n\nAND Operator");

let isLegalAge = true;
let isRegistered = false;


// Logical And Operator (&&)
// 		- returns true if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);



// Logical OR Operator (||)
// 		- return true if one of the operands are true
console.log("\n\nOR Operator");

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);




// Logical Not Operator (!)
// 		- returns the opposite value (Negates the value)
let someRequirementsNotMet = !isRegistered;

console.log("\n\nNOT Operator");
console.log("Result of logical NOT operator: " + someRequirementsNotMet);


/*
	Relational Operators:

		<	- Less than
		>	- Greater than
		<=	- Less than or equal to
		>=	- Greater than or equal to

*/
console.log("\n\nRelational Operator");
let a = 5;
let relate = a > 8;
console.log(relate);



// Selection Control Structures
// if, else if, and else statement
console.log("\n\nIf Statement");

let numG = -1;

// if Statement - executes a statement if a specified condition is true
/*
	Syntax:

		if(condition){
			statement/s;
		}

*/

if(numG < 0){
	console.log("Hello");
}


let numH = 1;
// else if clause - executes a statement if previous conditions are false and if the specified condition is true
console.log("\n\nElse If Clause");


if(numG > 0){
	console.log("Hello");
}

else if(numH === 0){
	console.log("World");
}

else if(numH > 0){
	console.log("Solar System");
}


// else clause - executes a statement if previous conditions are false
console.log("\n\nElse Clause");


if(numG > 0){
	console.log("Hello");
}

else if(numH === 0){
	console.log("World");
}

else{
	console.log("Try again");
}

/*
	Department A = 1 -> 3
	Department B = 4 -> 6
	Department C = 7 -> 9
*/

console.log("\n\nExample with two conditions");

let dept = 4;

if(dept >= 1 && dept <= 3){
	console.log("You're in Department A");
}

else if(dept >= 4 && dept <= 6){
	console.log("You're in Department B");
}

else if(dept >= 7 && dept <= 9){
	console.log("You're in Department C");
}

else{
	console.log("Department does not exist");
}




console.log("\n\nExample within function if statements");

let message = "No message";
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return "Not a typhoon yet.";
	}

	else if(windSpeed < 61){
		return "Tropical Depression detected.";
	}

	else if(windSpeed >= 62 && windSpeed <= 88){
		return "Tropical Storm detected.";
	}

	else if(windSpeed >= 89 && windSpeed <= 117){
		return "Severe Tropical Storm detected.";
	}

	else{
		return "Typhoon detected.";
	}
}


// Returns the string to the variable "message" that invoked it

message = determineTyphoonIntensity(66);

console.log(message);



// Truthy and Falsy
// 		- in JS a "truthy" value is a value that is considered true when encountered in Boolean context

/*
	Falsy values

		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN

*/
console.log("\n\nExamples Truthy and Falsy");

// Truthy
if(true){
	console.log('Truthy');
}

if(1){
	console.log('Truthy');
}

// Falsy example
	if(false){
		console.log('Falsey');
	}

	if(0){
		console.log('Falsey');
	}

	if(undefined){
		console.log('Falsey');
	}



// Single statement execution
// Conditional Ternary Operator
console.log("\n\nExamples Conditional Ternany Operator");

/*
	Syntax
		(expression) ? ifTrue : ifFlase;
*/

let ternaryResult = (1 < 18) ? 34 : "12";
console.log("Result of Ternary Operator: " + ternaryResult);


// let name;

// function isOfLegalAge(){
// 	name = 'John';
// 	return 'You are of the legal age limit';
// }

// function isUnderAge() {
// 	name = 'Jane';
// 	return 'You are under the age limit';
// }

// let age = parseInt(prompt("What is your age?"));

// console.log(age);

// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
// console.log("Result of Ternary Operator Funcitons: " + legalAge + ", " + name);	







// Switch Statements
/*
	Syntax

		switch (expression){
			case value1:
				statement/s;
				break;

			case value2:
				statement/s;
				break;

			case valueN:
				statement/s;
				break;

			default:
				statement/s;
		}
*/
console.log("\n\nSwitch Statements");


/*let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);


switch (day){
	case 'monday':
		console.log("The color of the day is red");
		break;

	case 'tuesday':
		console.log("The color of the day is orange");
		break;

	case 'wednesday':
		console.log("The color of the day is yellow");
		break;

	case 'thursday':
		console.log("The color of the day is green");
		break;

	case 'friday':
		console.log("The color of the day is blue");
		break;

	case 'saturday':
		console.log("The color of the day is indigo");
		break;

	case 'sunday':
		console.log("The color of the day is violet");
		break;

	default:
		console.log("Please input a valid day");
}*/



// Try - Catch - Finally Statement

//		- try catch statement are commonly used for error handling


// Finally block is used to specify a response/ action that is used to handle/ resolve errors
console.log("\n\nTry-Catch-Finally");


function showIntensityAlert(windSpeed) {
	try{
		alert(determineTyphoonIntensity());
	}

	catch (error){
		console.log(typeof error);
	}

	finally {
		alert("Intensity updates will show new alert.");
	}
}

showIntensityAlert(56);	